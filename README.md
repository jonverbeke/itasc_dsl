iTaSC DSL
=========

iTaSC DSL is a Domain Specific Language for constraint-based programming, more specifically iTaSC.
This repository contains the M2 model of iTaSC (itasc.lua) and example models (M1 level) under examples/.
iTaSC DSL is dual licensed under LGPL/BSD.

Dependencies
------------
- uMF: https://github.com/kmarkus/uMF
- OCL: http://www.orocos.org/toolchain (for utils.lua and rttros.lua, will be separated out in a future release)
- ROS environment (as package manager, rospack find tool)

Setup
-----
put in your .bashrc:

```sh
export LUA_PATH="$LUA_PATH;`rospack find uMF`/?.lua"
export LUA_PATH="$LUA_PATH;`rospack itasc_dsl`/?.lua"
export LUA_PATH="$LUA_PATH;`rospack itasc_dsl`/modules/?.lua"
export LUA_PATH="$LUA_PATH;`rospack find ocl`/lua/modules/?.lua"
```
- Export your uMF, iTaSC DSL and ocl location to the PATH variable

Model creation
--------------
To create a iTaSC application model and an executable from the template:
```sh
$ make_new_model.sh my_model
```
This script will make two files:
- my_model.lua: a copy or model_template.lua which you can edit
- run_my_model.lua: a simple executable to (verify and) deploy your model (using the iTaSC DSL to Orocos Deployer, http://bitbucket.org/dvanthienen/itasc_dsl_orocos_deployer/)

Modelchecking
-------------
To check an iTaSC model against the spec,
the model should be in file spec syntax: file://<absolute path to file>/<model file> or file://<ROS package name>#/<path from ROS pkg to file>/<model file> (in case of a ROS environment).
There are two options:

### LUA interactive mode ###
```sh
$ ./check_itasc_model.lua file://itasc_dsl#/examples/itasc_erf2012_demo_model.lua
```
Execute the following for more options

```sh
$ ./check_itasc_model.lua -h
```

### standard Orocos taskbrowser ###
*See `itasc\_dsl\_orocos\_deployer` package.*

Deploy the model using Orocos
-----------------------------
*See `itasc\_dsl\_orocos\_deployer` package.*

Contact
-------
Please direct questions, bugs or improvements to the orocos users (http://lists.mech.kuleuven.be/mailman/listinfo/orocos-users) mailing list.

More info and documentation
---------------------------
D. Vanthienen, M. Klotzbuecher, T. De Laet, J. De Schutter, and H. Bruyninckx, [Rapid application development of constrained-based task modelling and execution using domain specific languages,](https://lirias.kuleuven.be/bitstream/123456789/417427/2/itasc_dsl_iros2013.pdf) in Proc. IEEE/RSJ Int. Conf. Int. Robots and Systems, Tokyo, Japan, 2013, pp. 1860–1866.

Additional information on the underlying software architecture and the concepts behind this work, can be found in:
D. Vanthienen, M. Klotzbuecher, and H. Bruyninckx, [The 5C-based architectural Composition Pattern: lessons learned from re-developing the iTaSC framework for constraint-based robot programming](http://joser.unibg.it/index.php?journal=joser&page=article&op=view&path%5B%5D=78) in Journal of Software Engineering for Robotics, vol.5, no. 1, 2014, pp. 17-35

Acknowledgement
---------------
iTaSC DSL is developed at the Mechanical Engineering department of K.U.Leuven, Leuven, Belgium.
(c) KU Leuven 2013
The following people contributed to this release:
Dominick Vanthienen
Markus Klotzbuecher

Please refer to the paper mentioned under 'More info and documentation'.
Bibtex:
```
@inproceedings{     vanthienenIROS2013,
  author          = {Vanthienen, Dominick and Klotzbuecher, Markus and De~Laet,      Tinne and De~Schutter, Joris and Bruyninckx, Herman},
  title           = {Rapid application development of constrained-based task modelling and execution using Domain Specific Languages},
  booktitle       = {Proceedings of the 2013 IEEE/RSJ International Conference on Intelligent Robots and Systems},
  title           = {Proceedings of the 2013 IEEE/RSJ International Conference on Intelligent Robots and Systems},
  organization    = {IROS2013},
  year            = {2013},
  address         = {Tokyo, Japan}
  pages           = {1860--1866}
}
```
Bibtex of the paper with additional information:
```
@Article{            VanthienenKlotzbuecherBruyninckx2014,
  Author          = {Vanthienen, Dominick and Klotzb\"{u}cher, Markus and Bruyninckx, Herman},
  title           = {The {5C}-based architectural {C}omposition {P}attern: lessons learned from re-developing the {iTaSC} framework for constraint-based robot programming},  
  journal         = {Journal of Software Engineering for Robotics},
  year            = {2014},
  volume          = {5},
  number          = {1},
  pages           = {17--35},
  issn            = {2035-3928}
}
```

The research leading to these results has received funding from 
 the Flemish FWO project G040410N ('Autonome manipulatietaken met een vliegende robot' 'Autonomous manipulation using a flying robot'), as well as 
the European Community's Seventh Framework Programme (FP7/2007-2013) under grant agreement no. FP7-ICT-231940-BRICS (Best Practice in Robotics), 
Rosetta (2008-ICT-230902, Robot control for skilled execution of tasks in natural interaction with humans; based on autonomy, cumulative knowledge and learning), 
and RoboHow (FP7-ICT-288533 RoboHow.Cog). 
