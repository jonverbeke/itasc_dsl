iTaSC DSL examples
==================
As detailed and presented in the IROS 2013 paper:
"Rapid application development of constrained-based task modelling and execution using Domain Specific Languages", D. Vanthienen et al.

Example of some possible errors
-------------------------------
- `duplicate_name_error.lua`
- `incompatibility_error.lua`
- `non_existence_error_cpf.lua`
- `non_existence_error_robot.lua`
- `syntax_error.lua`
- `syntax_error_spelling.lua`
- `syntax_error_type.lua`
- `version_number_warning.lua`
