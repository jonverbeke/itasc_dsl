#!/usr/bin/env rttlua
-- -*- rttlua -*-

--This program checks a given model with the itasc meta-model

local umf=require("umf")         -- the itasc meta-meta-model
local itasc=require("itasc")     -- the itasc meta-model
local utils=require("utils")     -- tools and code snippets
local rttros = require("rttros") -- stupid dependency, just to have rospack find

Application       = itasc.Application
SetpointGenerator = itasc.SetpointGenerator
Sensor            = itasc.Sensor
iTaSC             = itasc.iTaSC
Robot             = itasc.Robot
Object            = itasc.Object
Rewire            = itasc.Rewire
SceneElement      = itasc.SceneElement
ExternalInput     = itasc.ExternalInput
Task              = itasc.Task
SkillTask         = itasc.SkillTask
TaskSetting       = itasc.TaskSetting
Solver            = itasc.Solver
Frame             = itasc.Frame
FSM               = itasc.FSM
VKC               = itasc.VKC
CC                = itasc.CC
Driver            = itasc.Driver
Rotation          = itasc.Rotation
Vector            = itasc.Vector

local instance_of = umf.instance_of
NumberSpec        = umf.NumberSpec
StringSpec        = umf.StringSpec
BoolSpec          = umf.BoolSpec
FunctionSpec      = umf.FunctionSpec
EnumSpec          = umf.EnumSpec
TableSpec         = umf.TableSpec
ClassSpec         = umf.ClassSpec
ObjectSpec        = umf.ObjectSpec

function help()
   print( [=[
check_itasc_model v0.2
validate itasc models.
  Usage:
     check_itasc_model OPTIONS <modelfile>
  <modelfile> should be written in file spec syntax, eg. file://my_ros_pkg#/folder/model.lua
  Options:
    -q    quiet, no output
    -h    print help
         ]=] )
end

function check_file(f, name)
   if not utils.file_exists(f) then
      print("error: could not find " .. name)
      os.exit(1)
   end
   return f
end

function filespec2file(filespec)
    local _,_,filename=string.find(filespec, "file://(.*)")

    if filename then
      -- if package
      local ret,_,pack,file = string.find(filename,"(.+)#(.+)")
      if ret~=nil then
         local packpath, res
         res, packpath = pcall(rttros.find_rospack, pack)
         if(string.find(file,"/(.*)")) then
           filename = packpath..file
         else
           filename = packpath.."/"..file
         end
      end
      if not utils.file_exists(filename) then
        print("non-existing file "..filename)
        os.exit(1)
      end 
    else
      print("Syntax error: Given file name '"..filespec.."' does not follow filespec syntax: file://ROSpackage#/path/to/file.extention")
      os.exit(1)
    end
    return filename
end

local argtab=utils.proc_args(arg)

if #arg<1 or argtab['-h'] then help(); os.exit(1); end

modelfile = filespec2file(arg[#arg])
print(check_file(modelfile))
model = dofile(modelfile)

if not model then 
    print("err: no model could be loaded from file " .. modelfile) 
    os.exit(1)
end

-------------------
-- conformity check
-------------------
if umf.check(model, itasc.Application_spec, (not argtab['-q']))~=0 then
   os.exit(1)
   print("Model does not conform to meta-model")
end

os.exit(0);
