my_tasks = {
   Task{
      name= "laser_tracing", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.laser_tracing',
      vkc = VKC{ type= "iTaSC::VKC_sixDof", package="sixDof_pff", 
        config={"file://itasc_erf2012_demo#/cpf/VKC_laser_tracing.cpf",
            {chain={"TransX","TransY","RotX","RotY","RotZ","TransZ"}}}},
      cc = CC{type="iTaSC::CC_sixDof_pff", package="sixDof_pff", 
	      config={"file://itasc_erf2012_demo#/cpf/CC_laser_tracing.cpf"} },
      fsm = FSM{fsm = "file://itasc_erf2012_demo#/scripts/laser_tracing_supervisor.lua", 
        config={"file://itasc_erf2012_demo#/cpf/laser_tracing_supervisor.cpf"}}},
   Task{
      name = "move_gripper_to_reference",
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.move_gripper_to_reference',
      vkc = VKC{type= "iTaSC::VKC_Cartesian", package="cartesian_motion"},
      cc = CC{
        type="iTaSC::CC_Cartesian", package="cartesian_motion",
        config={"file://itasc_erf2012_demo#/cpf/CC_cartesian_motion.cpf"}},
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}},
   Task{
      name = "pr2_joint_limit_avoidance",
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.pr2_joint_limit_avoidance',
      package = "joint_limit_avoidance",
      config = {"file://itasc_pr2#/cpf/pr2_jointlimits.cpf"},
      fsm = FSM{fsm = "file://joint_limit_avoidance#/scripts/joint_limit_avoidance_supervisor.lua"}},
} --end of tasks
return Application {
   dsl_version = '0.1',
   name = 'erf2012_demo_app',
   uri = 'be.kuleuven.mech.robotics.application.itasc_erf2012_demo',
   fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua", config={"file://itasc_erf2012_demo#/cpf/reduced_simulation_application_supervisor.cpf"}},
   -- itasc
   itasc = iTaSC {
      dsl_version = '0.1',
      name = 'erf2012_demo_itasc',
      uri = 'be.kuleuven.mech.robotics.itasc.itasc_erf2012_demo',
      fsm = FSM{fsm = "file://itasc_erf2012_demo#/scripts/reduced_itasc_supervisor.lua", config={"file://itasc_erf2012_demo#/cpf/reduced_itasc_supervisor.cpf"}},
      -- robots
      robots = {
    	Robot{
	        name = "pr2",
	        package = "itasc_pr2",
	        type = "iTaSC::pr2Robot",
            config = {"file://itasc_comanipulation_demo_app#/cpf/pr2robot.cpf"}}},
      -- objects
      objects = {
	    Object{
	        name = "wall",
	        package = "fixed_object",
	        type = "iTaSC::FixedObject"}},
      -- scene
      scene = {
	     SceneElement {
	        robot = "pr2",
	        location = Frame {
	           M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	           p = Vector{X=0.0,Y=0.0,Z=0.0}}},
	     SceneElement {
	        object = "wall",
	        location = Frame {
	           M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	           p = Vector{X=1.5,Y=0.0,Z=0.0}}}
      },
      tasks = my_tasks,
      task_settings = {
        TaskSetting{
          task = "laser_tracing",
          weight = 1.0,
          priority = 1,
	      o1 = "wall.ee",
          o2 = "pr2.r_gripper_tool_frame"
        },
        TaskSetting{
          task = "move_gripper_to_reference",
          weight = 1.0,
          priority = 1,
          o1 = "pr2.base_link",
          o2 = "pr2.r_gripper_tool_frame"
        },
        TaskSetting{
          task = "pr2_joint_limit_avoidance",
          weight = 1.0,
          priority = 1,
          robot = "pr2"
        }},
      solver = Solver{ name="Solver", package="wdls_prior_vel_solver", type="iTaSC::WDLSPriorVelSolver" }
   }, --end of iTaSC
   setpoint_generators = {
      SetpointGenerator{name="lissajous_generator", 
			type="trajectory_generators::LissajousGenerator",
			package="lissajous_generator",
            task="laser_tracing",
            config={"file://itasc_erf2012_demo#/cpf/lissajous_generator.cpf"}},
      SetpointGenerator{name="cartesian_generator", 
			type="trajectory_generators::CartesianGeneratorPos",
			package="cartesian_trajectory_generator",
            task="move_gripper_to_reference",
			config={ "file://itasc_erf2012_demo#/cpf/cartesian_generator.cpf"} }
   },
  drivers = { Driver{
    name="pr2_driver",
    robot="pr2",
    file="file://itasc_pr2#/scripts/pr2driver.lua"}}
} --end of application
