require "rfsm_ext"
require "rfsmpp"

local state, trans = rfsm.state, rfsm.transition

-- the FSM
return state {
    dbg=rfsmpp.gen_dbgcolor("composite task fsm", { STATE_ENTER=true, STATE_EXIT=true }, false),

    -- Toplevel entry: activate safety relevant tasks
    entry = function()
    raise_common_event('e_keep_joint_config_LimitJointPositions')
    raise_common_event('e_joint_limit_avoidance_AvoidJointLimits')
    end,
    -- reach handle
    reach = state {
        entry=function ()
            raise_common_event('e_reach_drawer_handle_sixDof_pffOn')
            moveToHandle()
        end,
        exit=function ()
            raise_common_event('e_reach_drawer_handle_DoNothing')
        end,
    },
    -- grip handle
    grip = state {
        entry=function ()
            grip()
        end
    },

    -- pull
    pull = state {
        entry = function ()
            raise_common_event('e_pull_drawer_handle_sixDof_pffOn')
        end,
        exit=function ()
            raise_common_event('e_pull_drawer_handle_DoNothing')
        end
    },

    idle = state {
        entry=function()
        end,
        exit=function()
        end,
    },

    trans{ src='initial', tgt='idle' },
    trans{ src='idle', tgt='reach', events={'e_start' } },
    trans{ src='reach', tgt='grip', events={'e_reach_drawer_trajectory_generator_move_finished' } },
    trans{ src='reach', tgt='idle', events={'e_stop' } },
    trans{ src='grip', tgt='pull', events={'e_done' } },
    trans{ src='grip', tgt='idle', events={'e_stop' } },
    trans{ src='pull', tgt='idle', events={'e_stop' } },
    trans{ src='pull', tgt='idle', events={'e_open_drawer_trajectory_generator_move_finished' } }
}
