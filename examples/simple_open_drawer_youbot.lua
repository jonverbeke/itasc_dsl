-- pull the drawer open part of the open drawer demo described in the IROS 2013 submission
-- task level
my_tasks = {
Task{
  name = "pull_drawer_handle",
  dsl_version = '0.1',
  uri = 'be.kuleuven.mech.robotics.task.pull_handle',
  vkc = VKC{ 
    type= "iTaSC::VKC_sixDof", 
    package="sixDof_pff", 
    config={
      "file://sixDof_pff#/cpf/VKC_sixDof_pff.cpf" ,
      {chain={"TransZ","RotZ","TransX","RotX","RotY","RotZ"}}}},
  cc = CC{type="iTaSC::CC_sixDof_pff", 
    package="sixDof_pff", 
    config={"file://sixDof_pff#/cpf/CC_sixDof_pff.cpf"}},
  fsm = FSM{
    fsm = "file://sixDof_pff#/scripts/sixDof_pff_supervisor.lua"}
},
Task{
  name = "keep_joint_config",
  dsl_version = '0.1',
  uri = 'be.kuleuven.mech.robotics.task.keep_jnt_cfg',
  cc = CC{
    type = "iTaSC::CC_PDFFjoints",
    package = "joint_motion"},
  fsm = FSM{
    fsm = "file://joint_motion#/scripts/joint_motion_supervisor.lua"}
}
}-- end of task level

-- iTaSC level
my_composite_task = iTaSC {
dsl_version = '0.1',
name = 'simple_open_drawer_task',
uri = 'be.kuleuven.mech.robotics.itasc.drawer_youbot',
fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_itasc_supervisor.lua"},
robots = { Robot{
    name = "youbot",
    package = "itasc_youbot",
    type = "iTaSC::youBot",
    config = {"file://itasc_erf2012_demo#/cpf/youbot.cpf"}}},
objects = { Object{
  name = "upper_drawer",
  package = "fixed_object",
  type = "iTaSC::FixedObject"}},
scene = {
  SceneElement {
    robot = "youbot",
    location = Frame {
      M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
      p = Vector{X=0.0,Y=0.0,Z=0.0}}},
  SceneElement {
    object = "upper_drawer",
	location = Frame {
	  M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	  p = Vector{X=2.8,Y=0.0,Z=0.5}}}},
task_settings = {
  TaskSetting{
    task = "pull_drawer_handle",
    weight = 1.0,
    priority = 1,
    o1 = "upper_drawer.handle", 
    o2 = "youbot.gripper_palm_link"},
  TaskSetting{
    task = "keep_joint_config",
    weight = 1.0,
    priority = 2,
    robot = "youbot"}},
solver = Solver{ 
  name="Solver", 
  package="wdls_prior_vel_solver", 
  type="iTaSC::WDLSPriorVelSolver"},
tasks = my_tasks
} -- end of iTaSC level

-- application level
return Application {
dsl_version = '0.1',
name = 'simple_open_drawer',
uri = 'be.kuleuven.mech.robotics.app.drawer_simyoubot',
fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua"},
setpoint_generators = {
  SetpointGenerator{
    name="open_drawer_trajectory_generator", 
    type="trajectory_generators::nAxesGeneratorPos",
    package="naxes_joint_generator",
    task="pull_drawer_handle",
    config={"file://itasc_dsl#/examples/cpf/open_drawer_traj_gen.cpf"}},
  SetpointGenerator{
    name="desired_joint_config_generator", 
    type="trajectory_generators::SimpleGenerator6D",
    package="simple_generator_6d",
    task="keep_joint_config",
    config={"file://itasc_dsl#/examples/cpf/desired_jnt_config.cpf"}}},
drivers = {
  Driver{
    name = "youbot_driver",
    package="youbot_description",
    robot= "youbot"}},
itasc = my_composite_task
} -- end of application level
