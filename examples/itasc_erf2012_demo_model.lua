my_tasks = {
   Task{
      name= "laser_tracing", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.laser_tracing',
      vkc = VKC{ type= "iTaSC::VKC_sixDof", package="sixDof_pff", 
        config={"file://itasc_erf2012_demo#/cpf/VKC_laser_tracing.cpf",
            {chain={"TransX","TransY","RotX","RotY","RotZ","TransZ"}}}},
      cc = CC{type="iTaSC::CC_sixDof_pff", package="sixDof_pff", 
	      config={"file://itasc_erf2012_demo#/cpf/CC_laser_tracing.cpf"} },
      fsm = FSM{fsm = "file://itasc_erf2012_demo#/scripts/laser_tracing_supervisor.lua", 
        config={"file://itasc_erf2012_demo#/cpf/laser_tracing_supervisor.cpf"}}},
   Task{
      name = "move_gripper_to_reference",
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.move_gripper_to_reference',
      vkc = VKC{type= "iTaSC::VKC_Cartesian", package="cartesian_motion"},
      cc = CC{
        type="iTaSC::CC_Cartesian", package="cartesian_motion",
        config={"file://itasc_erf2012_demo#/cpf/CC_cartesian_motion.cpf"}},
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}},
   Task{
      name = "youbot_joint_limit_avoidance",
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.youbot_joint_limit_avoidance',
      cc = CC{type= "iTaSC::CC_joint_limit_avoidance", package = "joint_limit_avoidance",
        config={"file://itasc_erf2012_demo#/cpf/CC_joint_limit_avoidance.cpf"}},
      fsm = FSM{fsm = "file://joint_limit_avoidance#/scripts/joint_limit_avoidance_supervisor.lua"}},
} --end of tasks
return Application {
   dsl_version = '0.1',
   name = 'erf2012_demo_app',
   uri = 'be.kuleuven.mech.robotics.application.itasc_erf2012_demo',
   fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua", config={"file://itasc_erf2012_demo#/cpf/reduced_simulation_application_supervisor.cpf"}},
   -- itasc
   itasc = iTaSC {
      dsl_version = '0.1',
      name = 'erf2012_demo_itasc',
      uri = 'be.kuleuven.mech.robotics.itasc.itasc_erf2012_demo',
      fsm = FSM{fsm = "file://itasc_erf2012_demo#/scripts/reduced_itasc_supervisor.lua", config={"file://itasc_erf2012_demo#/cpf/reduced_itasc_supervisor.cpf"}},
      -- robots
      robots = {
    	Robot{
	        name = "youbot",
	        package = "itasc_youbot",
	        type = "iTaSC::youBot",
            config = {"file://itasc_erf2012_demo#/cpf/youbot.cpf"}}},
      -- objects
      objects = {
	    Object{
	        name = "ceiling",
	        package = "fixed_object",
	        type = "iTaSC::FixedObject"}},
      -- scene
      scene = {
	     SceneElement {
	        robot = "youbot",
	        location = Frame {
	           M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	           p = Vector{X=0.0,Y=0.0,Z=0.0}}},
	     SceneElement {
	        object = "ceiling",
	        location = Frame {
	           M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	           p = Vector{X=0.0,Y=0.0,Z=2.0}}}
      },
      tasks = my_tasks,
      task_settings = {
        TaskSetting{
          task = "laser_tracing",
          weight = 1.0,
          priority = 1,
	      o1="ceiling.ee",
          o2="youbot.gripper_palm_link"
        },
        TaskSetting{
          task = "move_gripper_to_reference",
          weight = 1.0,
          priority = 1,
          o1 = "youbot.plate_link",
          o2 = "youbot.gripper_palm_link"
        },
        TaskSetting{
          task = "youbot_joint_limit_avoidance",
          weight = 1.0,
          priority = 1,
          robot = "youbot"
        }},
      solver = Solver{ name="Solver", package="wdls_prior_vel_solver", type="iTaSC::WDLSPriorVelSolver" }
   }, --end of iTaSC
   setpoint_generators = {
      SetpointGenerator{name="lissajous_generator", 
			type="trajectory_generators::LissajousGenerator",
			package="lissajous_generator",
            task="laser_tracing",
            config={"file://itasc_erf2012_demo#/cpf/lissajous_generator.cpf"}},
      SetpointGenerator{name="cartesian_generator", 
			type="trajectory_generators::CartesianGeneratorPos",
			package="cartesian_trajectory_generator",
            task="move_gripper_to_reference",
			config={ "file://itasc_erf2012_demo#/cpf/cartesian_generator.cpf"} }
   },
   drivers = {
      Driver{
            name = "youbot_driver", 
            package="youbot_description",
            robot="youbot"}}
} --end of application
