-- pull the drawer open part of the open drawer demo described in the IROS 2013 submission
-- task level
my_tasks = CompositeTask{
  name = "simple_open_drawer",
  dsl_version = "0.2",
  uri = "be.kuleuven.mech.rob.task.simple_open_drawer",
  tasks = {
  Task{
    name = "pull_drawer_handle",
    dsl_version = "0.2",
    uri = "be.kuleuven.mech.rob.task.pull_handle",
    vkc = VKC{ 
      name = "cylindrical_coordinates",
      dsl_version = "0.2",
      uri = "be.kuleuven.mech.rob.vkc.cylindrical",  
      type= "iTaSC::VKC_sixDof", 
      package = "sixDof_pff"
      chain = {"TransZ","RotZ","TransX","RotX","RotY","RotZ"}},
    cc = CC{
      name = "sixdof_pff",
      dsl_version = "0.2",
      uri = "be.kuleuven.mech.rob.cc.sixdof_pff",   
      type="iTaSC::CC_sixDof_pff", 
      package = "sixDof_pff"},
    sg = SetpointGenerator{
      name = "desired_joint_config_generator", 
      dsl_version = "0.2",
      uri = "be.kuleuven.mech.rob.sg.simple6d",   
      type = "trajectory_generators::SimpleGenerator6D"
      package = "simple_generator_6d"},
    coord = Coordinator{
      fsm = "file://sixDof_pff_supervisor.lua"},
    config = Configurator{
      conf = "file://pull_drawer_handle.lua"}},
  Task{
    name = "keep_joint_config",
    dsl_version = "0.2",
    uri = "be.kuleuven.mech.rob.task.keep_jnt_cfg"
    cc = CC{
      name = "pdff_joints",
      dsl_version = "0.2",
      uri = "be.kuleuven.mech.rob.cc.pdff_joints",
      type = "iTaSC::CC_PDFFjoints",
      package = "joint_motion"},
    sg = SetpointGenerator{
      name = "open_drawer_trajectory_generator", 
      dsl_version = "0.2",
      uri = "be.kuleuven.mech.rob.sg.naxesgeneratorpos",
      type = "trajectory_generators::nAxesGeneratorPos"
      package = "naxes_joint_generator"},  
    coord = Coordinator{
      fsm = "file://jnt_config_supervisor.lua"},
    config = {"file://keep_joint_config.cpf"}},
  coord = Coordinator{fsm = "file://composite_task_supervisor.lua"},
  config = Configurator{
    conf = "file://simple_open_drawer.lua",
    config = {
      TaskSetting{
        task = "pull_drawer_handle",
        weight = 1.0,
        priority = 1},
      TaskSetting{
        task = "keep_joint_config",
        weight = 1.0,
        priority = 2}}}
  } -- end of tasks
}-- end of my_tasks

my_world = WorldModel {
name = "experimental_setup",
dsl_version = "0.2",
uri = "be.kuleuven.mech.rob.worlds.experimental_setup",
robots = {
  Robot{
    name = "pr2",
    dsl_version = "0.2",
    uri = "be.kuleuven.mech.rob.robots.pr2"
    package = "itasc_pr2",
    type = "iTaSC::pr2Robot"}},
objects = {
  Object{
    name = "upper_drawer",
    dsl_version = '0.2',
    uri = "be.kuleuven.mech.rob.objects.furniture.drawer.upper_drawer"  
    package = "fixed_object",
    type = "iTaSC::FixedObject"}},
scene = Scene{
  name = "robolab",
  dsl_version = "0.2",
  uri = "be.kuleuven.mech.rob.scene.robolab",
  package = "scenes",
  type = "iTaSC::Scene"
},
coord = Coordinator {fsm = "file://experimental_setup.lua"},
config = {
  SceneElement {
    name = "start_loc",
    location = Frame {
        M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
        p = Vector{X=0.0,Y=0.0,Z=0.0}}},
  SceneElement {
    name = "drawer_pos",
  location = Frame {
    M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
    p = Vector{X=2.8,Y=0.0,Z=0.5}}},
  "file://experimental_setup.cpf"},
compo = {
  ObjectFrameConn{
    name = "conn_w_pr2odom",
    fr1 = "robolab.start_loc", 
    fr2 = "pr2.odom"},
  ObjectFrameConn{
    name = "conn_w_drawerbase",
    fr1 = "robolab.drawer_pos", 
    fr2 = "upper_drawer.base"}
}} -- end of my_world
-- end of task level

-- CBP level
my_constraint_based_program = CBP {
name = "itasc_simple_open_drawer_pr2",
dsl_version = "0.2",
uri = "be.kuleuven.mech.rob.cbp.drawer_pr2",
world_model = my_world,
solver = Solver{ 
  name = "Solver",
  dsl_version = "0.2",
  uri = "be.kuleuven.mech.rob.solver.wdls_prior_vel_solver",
  package = "wdls_prior_vel_solver", 
  type = "iTaSC::WDLSPriorVelSolver"},
tasks = my_tasks,
coord = Coordinator{fsm = "file://cbp_supervisor.lua"},
config= {"file://cbp_supervisor.cpf"},
compo = {
  ObjectFrameConn{
    name = "conn_pull_drawer",
    fr1 = "simple_open_drawer.pull_drawer_handle.o1", 
    fr2 = "experimental_setup.upper_drawer.handle"},
  ObjectFrameConn{
    name = "conn_pull_rgripper",
    fr1 = "simple_open_drawer.pull_drawer_handle.o2", 
    fr2 = "experimental_setup.pr2.r_gripper_tool_frame"},
  ObjectFrameConn{
    name = "conn_keepjnt_pr2",
    chain = "simple_open_drawer.keep_joint_config",
    robot = "experimental_setup.pr2"}
}} -- end of CBP level

-- application level
return Application {
name = "simple_open_drawer_using_real_pr2",
dsl_version = "0.2",
uri = "be.kuleuven.mech.rob.app.drawer_simpr2",
platforms = {
  Platform{
    name = "pr2_hardware", 
    dsl_version = "0.2",
    uri = "be.kuleuven.mech.rob.platform.pr2",
    file = "file://pr2driver.lua"
    }},
cbp = my_constraint_based_program,
coord = Coordinator{fsm = "file://app_supervisor.lua"},
config = {"file://simple_open_drawer_using_real_pr2.cpf"},
compo = {
  EntityConn{
    name = "conn_pr2hw_pr2",
    e1 = "pr2_hardware", 
    e2 = "itasc_simple_open_drawer_pr2.experimental_setup.pr2"}}
} -- end of application level
