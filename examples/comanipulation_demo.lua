my_tasks = {
   Task{
      name= "grippers_parallel", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.grippers_parallel',
      vkc = VKC{ type= "iTaSC::VKC_Cartesian", package="cartesian_motion"}, 
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion", 
	    config={"file://keep_parallel#/cpf/CC_KeepParallel.cpf"} },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}},
   Task{
      name= "obstacle_avoidance", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.obstacle_avoidance',
      vkc = VKC{ type= "iTaSC::VKC_sixDof_pff", package="sixDof_pff", 
        config={"file://keep_distance#/cpf/VKC_KeepDistance.cpf"}},
      cc = CC{type="iTaSC::CC_sixDof", package="sixDof_pff", 
	    config={"file://keep_distance#/cpf/CC_KeepDistance.cpf"} },
      fsm = FSM{fsm = "file://sixDof_pff#/scripts/sixDof_pff_supervisor.lua"}},
   Task{
      name= "head_tracking", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.head_tracking',
      vkc = VKC{ type= "iTaSC::VKC_Cartesian", package="cartesian_motion"}, 
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion", 
	    config={"file://head_following#/cpf/CC_HeadFollowing.cpf"} },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}},
   Task{
      name= "wrench_nulling_left", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.wrench_nulling_left',
      vkc = VKC{ type= "iTaSC::VKC_Cartesian", package="cartesian_motion"}, 
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion", 
	    config={"file://itasc_comanipulation_demo_app#/cpf/CC_ForceNullingLeft.cpf"} },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}},
   Task{
      name= "wrench_nulling_right", 
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.wrench_nulling_right',
      vkc = VKC{ type= "iTaSC::VKC_Cartesian", package="cartesian_motion"}, 
      cc = CC{type="iTaSC::CC_Cartesian", package="cartesian_motion", 
	    config={"file://itasc_comanipulation_demo_app#/cpf/CC_ForceNullingRight.cpf"} },
      fsm = FSM{fsm = "file://cartesian_motion#/scripts/cartesian_motion_supervisor.lua"}},
   Task{
      name = "pr2_joint_limit_avoidance",
      dsl_version = '0.1',
      uri = 'be.kuleuven.mech.robotics.task.pr2_joint_limit_avoidance',
      package = "joint_limit_avoidance",
      config = {"file://itasc_pr2#/cpf/pr2_jointlimits.cpf"},
      fsm = FSM{fsm = "file://joint_limit_avoidance#/scripts/joint_limit_avoidance_supervisor.lua"}},
} --end of tasks
return Application {
   dsl_version = '0.1',
   name = 'human_pr2_comanipulation_demo_real',
   uri = 'be.kuleuven.mech.robotics.application.human_pr2_comanipulation_demo',
   fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_application_supervisor.lua"},
   -- itasc
   itasc = iTaSC {
      dsl_version = '0.1',
      name = 'human_pr2_comanipulation_demo',
      uri = 'be.kuleuven.mech.robotics.itasc.human_pr2_comanipulation_demo',
      fsm = FSM{fsm = "file://itasc_core#/scripts/reduced_itasc_supervisor.lua", config = {"file://itasc_comanipulation_demo_app#/cpf/itasc_supervisor.cpf"}},
      -- robots
      robots = {
    	Robot{
	        name = "pr2",
	        package = "itasc_pr2",
	        type = "iTaSC::pr2Robot",
            config = {"file://itasc_comanipulation_demo_app#/cpf/pr2robot.cpf"}}},
      -- objects
      objects = {
	    Object{
	        name = "obstacle",
	        package = "fixed_object",
	        type = "iTaSC::FixedObject",
            config = {"file://itasc_comanipulation_demo_app#/cpf/FixedObject.cpf"}},
	    Object{
	        name = "person",
	        package = "moving_object",
	        type = "iTaSC::MovingObject",
            config = {"file://itasc_comanipulation_demo_app#/cpf/MovingObject.cpf"}}},
      -- scene
      scene = {
	     SceneElement {
	        robot = "pr2",
	        location = Frame {
	           M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	           p = Vector{X=0.0,Y=0.0,Z=0.0}}},
	     SceneElement {
	        object = "obstacle",
	        location = Frame {
	           M = Rotation{X_x=1,Y_x=0,Z_x=0,X_y=0,Y_y=1,Z_y=0,X_z=0,Y_z=0,Z_z=1},
	           p = Vector{X=2.8,Y=0.0,Z=0.0}}},
	     SceneElement {
	        object = "person",
            location = ExternalInput{topic = "/find_face/face_world"}}},
      tasks = my_tasks,
      task_settings = {
        TaskSetting{
          task = "grippers_parallel",
          weight = 1.0,
          priority = 1,
	      o1="pr2.l_gripper_tool_frame", 
          o2="pr2.r_gripper_tool_frame"},
        TaskSetting{
          task = "obstacle_avoidance",
          weight = 1.0,
          priority = 1,
	      o1="obstacle.ee", 
          o2="pr2.base_link"},
        TaskSetting{
          task = "head_tracking",
          weight = 1.0,
          priority = 1,
	      o1="pr2.head_plate_frame",
          o2="person.ee"},
        TaskSetting{
          task = "wrench_nulling_left",
          weight = 1.0,
          priority = 1,
	      o1="pr2.root",
          o2="pr2.l_gripper_tool_frame"},
        TaskSetting{
          task = "wrench_nulling_right",
          weight = 1.0,
          priority = 1,
	      o1="pr2.root",
          o2="pr2.r_gripper_tool_frame"},
        TaskSetting{
          task = "pr2_joint_limit_avoidance",
          weight = 1.0,
          priority = 1,
          robot = "pr2"}},
      solver = Solver{ name="Solver", package="wdls_prior_vel_solver", type="iTaSC::WDLSPriorVelSolver" }
   }, --end of iTaSC
   setpoint_generators = {
      SetpointGenerator{name="parallel_pose_generator",
			type="trajectory_generators::fixed_pose_generator",
			package="fixed_pose_generator",
            task="grippers_parallel",
            config={"file://itasc_comanipulation_demo_app#/cpf/ParallelPoseGenerator.cpf"} },
      SetpointGenerator{name="head_following_generator", 
			type="trajectory_generators::fixed_pose_generator",
			package="fixed_pose_generator",
            task="head_tracking",
            config={"file://itasc_comanipulation_demo_app#/cpf/HeadFollowingGenerator.cpf"} },
      SetpointGenerator{name="keep_distance_generator", 
			type="trajectory_generators::SimpleGenerator6D",
			package="simple_generator_6d",
            task="obstacle_avoidance",
            config={"file://itasc_comanipulation_demo_app#/cpf/KeepDistanceGenerator.cpf"} },
      SetpointGenerator{name="left_twist_generator", 
			type="trajectory_generators::tdFromForceGenerator",
			package="td_from_force_generator",
            task="wrench_nulling_left",
            config={"file://itasc_comanipulation_demo_app#/cpf/twistGenerator.cpf"} },
      SetpointGenerator{name="right_twist_generator", 
			type="trajectory_generators::tdFromForceGenerator",
			package="td_from_force_generator",
            task="wrench_nulling_right",
            config={"file://itasc_comanipulation_demo_app#/cpf/twistGenerator.cpf"} },
  }, --end of setpoint_generators 
  drivers = { Driver{
    name="pr2_driver", 
    robot="pr2",
    file="file://itasc_pr2#/scripts/pr2driver.lua"}}
} --end of application
