-- iTaSC meta-model
-- This file is part of iTaSC DSL.
--
-- (C) 2012, 2013, 2014 Dominick Vanthienen, dominick.vanthienen@kuleuven.be,
-- Department of Mechanical Engineering, Katholieke Universiteit
-- Leuven, Belgium.
--
-- You may redistribute this software and/or modify it under either
-- the terms of the GNU Lesser General Public License version 2.1
-- (LGPLv2.1 <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>)
-- or (at your discretion) of the Modified BSD License: Redistribution
-- and use in source and binary forms, with or without modification,
-- are permitted provided that the following conditions are met:
-- 1. Redistributions of source code must retain the above copyright
-- notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
-- copyright notice, this list of conditions and the following
-- disclaimer in the documentation and/or other materials provided
-- with the distribution.
-- 3. The name of the author may not be used to endorse or promote
-- products derived from this software without specific prior
-- written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
-- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
-- WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
-- NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--

require "umf"
require "rttros"
require "strict"

-- DSL version number
local global_dsl_version = '0.2'

-- some handy shortcuts
local NumberSpec, StringSpec, TableSpec, ObjectSpec, AnySpec, EnumSpec = umf.NumberSpec, umf.StringSpec, umf.TableSpec, umf.ObjectSpec, umf.AnySpec, umf.EnumSpec

local ts = tostring
local instance_of = umf.instance_of
local ind_inc, ind_dec =umf.ind_inc, umf.ind_dec

-- umf model
module("itasc", package.seeall)

-- entity definition
-- -- general composition pattern
Coordinator = umf.class("Coordinator")
Configurator = umf.class("Configurator")
Composer = umf.class("Composer")

-- -- application level
Application = umf.class("Application")
Platform = umf.class("Platform")
Sensor = umf.class("Sensor")
CBP = umf.class("CBP")

-- -- cbp level
WorldModel = umf.class("WorldModel")
Solver = umf.class("Solver")
CompositeTask = umf.class("CompositeTask")

-- -- task level
Scene = umf.class("Scene")
Robot = umf.class("Robot")
Object = umf.class("Object")

Task = umf.class("Task")
VKC = umf.class("VKC")
CC = umf.class("CC")
SetpointGenerator = umf.class("SetpointGenerator")

-- -- other levels
TaskSetting = umf.class("TaskSetting")
Frame = umf.class("frame")
Rotation = umf.class("Rotation")
Vector = umf.class("Vector")
FSM = umf.class("FSM")
EntityConn = umf.class("EntityConn")
ObjectFrameConn = umf.class("ObjectFrameConn")
SceneElement = umf.class("SceneElement")
ExternalInput = umf.class("ExternalInput")

-- predicates
-- -- general composition pattern 
function is_FSM(x) 
    return umf.uoo_type(x) == 'instance' and instance_of(FSM,x)
end

function is_Coordinator()
    return umf.uoo_type(x) == 'instance' and instance_of(Coordinator,x)
end

function is_Configurator()
    return umf.uoo_type(x) == 'instance' and instance_of(Configurator,x)
end

function is_Composer()
    return umf.uoo_type(x) == 'instance' and instance_of(Composer,x)
end

-- -- application level
-- -- cbp level
-- -- task level
-- -- other levels
function is_SceneElement(x) return umf.uoo_type(x) == 'instance' and instance_of(SceneElement, x) end
function is_SceneElement_Task(x) return umf.uoo_type(x) == 'instance' and (instance_of(SceneElement, x) or instance_of(Task, x)) end
function is_SceneElement_TaskSetting_Platform(x) return umf.uoo_type(x) == 'instance' and (instance_of(SceneElement, x) or instance_of(TaskSetting, x) or instance_of(Platform, x)) end
function is_Robot(x) return umf.uoo_type(x) == 'instance' and instance_of(Robot, x) end
function is_table(x) return type(x) == 'table' end
function is_table_notFrame(x) return type(x) == 'table' and not (umf.uoo_type(x) == 'instance' and ( instance_of(Frame,x) or instance_of(Rotation,x) or instance_of(Vector,x))) end 
function is_TaskSetting(x)
    return umf.uoo_type(x) == 'instance' and instance_of(TaskSetting,x)
end
function is_package(x) 
    return umf.uoo_type(x) and x.package
end

function is_component(x) 
    return umf.uoo_type(x) and x.type
end

function is_Task(x) 
    return umf.uoo_type(x) == 'instance' and instance_of(Task, x) 
end

function is_Robot_Object(x) 
    return umf.uoo_type(x) == 'instance' and (instance_of(Robot, x) or instance_of(Object, x)) 
end

function is_Rotation(x) 
    return umf.uoo_type(x) == 'instance' and instance_of(Rotation, x) 
end

function is_CC(x)
    return umf.uoo_type(x) == 'instance' and instance_of(CC, x)
end

function is_VKC(x)
    return umf.uoo_type(x) == 'instance' and instance_of(VKC, x)
end

function is_VKC_Task(x)
    return umf.uoo_type(x) == 'instance' and (instance_of(VKC, x) or instance_of(Task, x))
end

function is_Solver(x)
    return umf.uoo_type(x) == 'instance' and instance_of(Solver, x)
end

function is_component_or_FSM(x)
    return umf.uoo_type(x) == 'instance' and (x.type or instance_of(FSM,x))
end

function is_SetpointGenerator(x)
    return umf.uoo_type(x) == 'instance' and instance_of(SetpointGenerator, x)
end

function is_Platform(x)
    return umf.uoo_type(x) == 'instance' and instance_of(Platform, x)
end

function is_applicationlevel_component(x) 
    return is_component(x) and not is_cbpORtasklevel_component(x)
end

function is_applicationlevel_component_or_supervisor(x)
    return is_applicationlevel_component(x) or (umf.uoo_type(x) == 'instance' and x._supervisor)
end

function is_cbpORtasklevel_component(x) 
    return umf.uoo_type(x) and x.type and 
      ((umf.uoo_type(x._parent) and instance_of(CBP, x._parent)) or 
      (umf.uoo_type(x._parent._parent) and instance_of(CBP,x._parent._parent)) or
      (umf.uoo_type(x._parent._parent._parent) and instance_of(CBP, x._parent._parent._parent)))
end

function is_tasklevel_component(x) 
    return umf.uoo_type(x) and x.type and 
      ((umf.uoo_type(x._parent) and instance_of(Task, x._parent)) or 
      (umf.uoo_type(x._parent._parent) and instance_of(Task,x._parent._parent)))
end

function is_cbplevel_component(x) 
    return is_cbpORtasklevel_component(x) and not is_tasklevel_component(x)
end

function is_tasklevel_FSM(x)
    return is_FSM(x) and umf.uoo_type(x._parent) and instance_of(Task, x._parent)
end

function is_cbplevel_FSM(x)
    return is_FSM(x) and umf.uoo_type(x._parent) and instance_of(CBP, x._parent)
end

function is_applicationlevel_FSM(x)
    return is_FSM(x) and umf.uoo_type(x._parent) and instance_of(Application, x._parent)
end

function has_config(x)
    return umf.uoo_type(x) == 'instance' and x.config
end

function has_driver(x)
    return umf.uoo_type(x) == 'instance' and x.driver
end

function has_compositelevel_supervisor(x)
    return umf.uoo_type(x) == 'instance' and x._supervisor 
    and ((umf.uoo_type(x._parent) and instance_of(Application, x._parent))
    or ( not (umf.uoo_type(x._parent)) and
    umf.uoo_type(x._parent._parent) and instance_of(Application, x._parent._parent)))
end

function is_ExternalInput(x)
    return umf.uoo_type(x) == 'instance' and instance_of(ExternalInput, x)
end

-- check if a table key is metadata (for now starts with a '_')
function is_meta(key) return string.sub(key, 1, 1) == '_' end

-- ROSPackSpec
ROSPackSpec = umf.class("RosPackSpec", umf.StringSpec)

-- Replace with comp_spec: Table spec with package, type. Check
-- validates that pack is valid package and that after importing it a
-- Component of Type is available?
-- @param self the specification
-- @param obj the object to check using the specification
-- @param vres data structure to report the result (error)

function ROSPackSpec.check(self, obj, vres)
   ind_inc()
   umf.log("validating object against ROSPackSpec")
   umf.vres_push_context(vres, self.name)
   local res = umf.StringSpec.check(self, obj, vres)

   if not res then
      umf.vres_pop_context(vres)
      return false
   end

   res = pcall(rttros.find_rospack, obj)
   if not res then
      umf.add_msg(vres, "err", tostring(obj) .. " not a known ROS package")
   end
   umf.log("validated object against ROSPackSpec, result: "..tostring(res))
   umf.vres_pop_context(vres)
   ind_dec()
   return res
end

rospack_spec = ROSPackSpec{ name='rospack_spec' } --StringSpec{} 

--VersionSpec
VersionSpec = umf.class("VersionSpec", umf.StringSpec)
--- Validate and expand version spec
-- This function checks whether the given version number is correct
function VersionSpec.check(self, obj, vres)
   umf.log("Validating object against VersionSpec")
   
   local res = umf.StringSpec.check(self, obj, vres)
   if not res then
      return false
   end
   
   if self.name == "cbp_dsl_version_spec" then
     if obj~=cbp_dsl_version then
       umf.add_msg(vres,"warn", "Current CBP metamodel version number "..ts(cbp_dsl_version)..", does not match required version number "..ts(obj))
       return false
     end
   else 
     if self.name == "application_dsl_version_spec" then
       if obj~=application_dsl_version then
         umf.add_msg(vres,"warn", "Current Application metamodel version number "..ts(application_dsl_version)..", does not match required version number "..ts(obj))
         return false
       end
     else
        umf.add_msg(vres,"err", ts(obj).." is an unknown version type")
        return false
     end
   end
   
   return res
end 

cbp_dsl_version_spec = VersionSpec{name="cbp_dsl_version_spec"}
application_dsl_version_spec = VersionSpec{name="application_dsl_version_spec"}

--FileSpec
FileSpec = umf.class("FileSpec", umf.StringSpec)
--- Validate and expand file spec.
-- This function expands the package# syntax and checks wether
-- files exists.
function FileSpec.check(self, obj, vres)
   umf.log("validating object against FileSpec")

   local res = umf.StringSpec.check(self, obj, vres)
   if not res then
      return false
   end

   local _,_,filename=string.find(obj, "file://(.*)")
   if not filename then 
print(obj)
      umf.add_msg(vres, "err", "invalid filespec " .. obj)
      return false
   end

   -- package local syntax
   local ret,_,pack,file = string.find(filename,"(.+)#(.+)")
   if ret~=nil then
      local packpath
      res, packpath = pcall(rttros.find_rospack, pack)
      if not res then
         umf.add_msg(vres, "err", "rospack_find failed to locate package "..pack)
      end
      local retn,_,_ = string.find(file,"/(.*)")
      if retn then
        filename = packpath..file
      else
        filename = packpath.."/"..file
      end
   end

   if not utils.file_exists(filename) then
      umf.add_msg(vres, "err", "non-existing configuration file "..filename.. "("..obj..")")
      res = false
   end

   -- determine file extension (not needed for check)
   --local _,_,fileext=string.find(filename, "%.(%a+)$")
   return res
end

--LuaFileSpec
LuaFileSpec = umf.class("LuaFileSpec", FileSpec)
--- Validate and expand lua file spec.
-- This function expands the package# syntax and checks wether
-- files exists.
function LuaFileSpec.check(self, obj, vres)
   umf.log("validating object against LuaFileSpec")
   -- check whether it conforms to the FileSpec
   local res = self:class():super().check(self, obj, vres)
   if not res then
      return false
   end
   -- determine file extension (not needed for check)
   local _,_,fileext=string.find(obj, "%.(%a+)$")
   if not fileext then 
      umf.add_msg(vres, "err", obj.. " has no file extention, expected .lua")
      return false
   elseif fileext ~= 'lua' then
      umf.add_msg(vres, "err", obj.. " has file extention "..tostring(fileext)..", expected .lua")
      return false
   end
   return res
end

--mk:
--FileSpec = StringSpec --uncomment in case you don't want to check the existance of all files
rotation_spec=ObjectSpec{
   name='kdl_rotation',
   sealed='both',
   type=Rotation,
   dict = { 
      X_x = NumberSpec{}, Y_x = NumberSpec{}, Z_x = NumberSpec{},
      X_y = NumberSpec{}, Y_y = NumberSpec{}, Z_y = NumberSpec{},
      X_z = NumberSpec{}, Y_z = NumberSpec{}, Z_z = NumberSpec{}
   }
}

vector_spec=ObjectSpec{
	name='kdl_vector',
    type=Vector,
	sealed='both',
	dict={ X = NumberSpec{}, Y = NumberSpec{}, Z = NumberSpec{} }
}

--- Frame
frame_spec = ObjectSpec{
   name='frame',
   type=Frame,
   sealed='both',

   dict={
        M=rotation_spec,
        p=vector_spec
   }
}

chain_spec = TableSpec {
   name='vkc_chain',
   sealed='both',
   array={ EnumSpec{"TransX", "TransY", "TransZ", "RotX", "RotY", "RotZ" } },
   postcheck=function(self, obj, vres)
		local ret = true
		-- must define exactly 6 elements
		if #obj ~= 6 then
		   umf.add_msg(vres, "err", "invalid number of chain segments (should be 6)")
		   return false
		end

		-- check that no segments are consecutive
		for i=2,6 do
		   if obj[i]==obj[i-1] then
		      umf.add_msg(vres, "err", "identical consecutive chain segments ("
				  ..ts(i-1).."-"..ts(i)..") found")
		      ret=false
		   end
		end
		return ret
	     end,
}

constraint_operator_spec = TableSpec{
   name='constraint_operator',
   sealed='both',
   array={ EnumSpec{"none", "equality", "greater", "lesser"} },
}

control_type_spec = TableSpec{
   name='control_type',
   sealed='both',
   array={ EnumSpec{"pos", "impedance", "force"} },
}

config_spec = TableSpec{
   name='config',
   sealed = 'both',
   array = {
      FileSpec{},
      TableSpec{
      	 name="property_table",
      	 sealed='none',
      	 dict={ 
           chain = chain_spec,
           nc = NumberSpec{},
           constraint_operator = constraint_operator_spec,
           control_type = control_type_spec
         },
      	 optional={'chain','nc','constraint_operator','control_type'},
      }
   },
}

array_spec = TableSpec {
   name='array_spec',
   sealed='both',
   array={ NumberSpec{} }
}

SPconfig_spec = TableSpec{
   name='config',
   sealed = 'both',
   array = {
      FileSpec{},
      TableSpec{
      	 name="property_table",
      	 sealed='none',
      	 dict={ 
           constraint_value = array_spec,
           execution_time = array_spec,
           maximum_velocity = array_spec,
           maximum_acceleration = array_spec,
         },
      	 optional={'constraint_value','execution_time','maximum_velocity','maximum_acceleration'},
      }
   },
}

-- Robot
robot_spec = ObjectSpec{
   name = 'itasc_robot',
   type = Robot,
   sealed = 'both',
   dict = {
      name=umf.StringSpec{},
      type=StringSpec{},
      package=rospack_spec,
      config = config_spec,
   },
   optional = {"config"}
}

-- Object
object_spec = ObjectSpec{
   name = 'CBP_object',
   type = Object,
   sealed = 'both',
   dict = {
      name=StringSpec{},
      type=StringSpec{},
      package=rospack_spec,
      config = config_spec
   },
   optional = {"config"}
}

external_input_spec = ObjectSpec {
   name='external_input',
   type=ExternalInput,
   sealed='both',
   dict = { 
     name=StringSpec{},
     topic=StringSpec{}
   },
   optional = {"name","topic"}
}

LocationSpec = umf.class("LocationSpec", umf.Spec)

function LocationSpec.check(self, obj, vres)
   local ret = true
   ind_inc()
   umf.log("validating object against LocationSpec")
   if not umf.uoo_type(obj) then
      umf.add_msg(vres, "err", tostring(obj) .. " not an UMF object")
      ind_dec()
      return false
   end

   if instance_of(Frame, obj) then
      ret = frame_spec:check(obj, vres)
   elseif instance_of(ExternalInput, obj) then
      ret = external_input_spec:check(obj, vres)
   else
      umf.add_msg(vres, "err", "expected Frame or ExternalInput, got:"..tostring(obj))
      ret=false
   end

   ind_dec()
   return ret
end

-- Scene
scene_element_spec = ObjectSpec{
   name = 'SceneElement',
   type = SceneElement,
   sealed = 'both',
   dict = {
      object=StringSpec{},
      robot=StringSpec{},
      location = LocationSpec{},
   },
   optional = { "object", "robot" },
}

-- VKC spec
vkc_spec = ObjectSpec{
   name = 'VKC',
   type = VKC,
   sealed = 'both',
   dict = {
      --name=StringSpec{},
      type=StringSpec{},
      package=rospack_spec,
      config = config_spec,
   },
   optional = { "config" },
}

CCconfig_spec = TableSpec{
   name='config',
   sealed = 'both',
   array = {
      FileSpec{},
      TableSpec{
      	 name="property_table",
      	 sealed='none',
      	 dict={ 
           weight = array_spec,
           proportional_gain = array_spec,
         },
      	 optional={'weight','proportional_gain'},
      }
   },
}

-- CC spec
cc_spec = ObjectSpec{
   name = 'CC',
   type = CC,
   sealed = 'both',
   dict = {
      -- name=StringSpec{},
      type=StringSpec{},
      package=rospack_spec,
      config = CCconfig_spec
   },
   -- need to check that either one is defined, though.
   optional = { "config" },
}

-- FSM spec
fsm_spec = ObjectSpec{
    name = 'FSM',
    type = FSM,
    sealed = 'both',
    dict = {
        fsm=FileSpec{},
        config = config_spec
    },
    optional = {"config"}
}


-- rewire spec OBSOLETE
rewire_spec = ObjectSpec{
    name = 'rewire',
    type = Rewire,
    sealed = 'both',
    dict = {
	   src=StringSpec{},
	   tgt=StringSpec{}
    },
    array = {
       TableSpec{ name="rewire_entry",
		  sealed='both',
		  dict={from=StringSpec{}, to=StringSpec{}}
	       }
    }
 }

-- connect spec OBSOLETE
connect_spec = TableSpec{
   name = 'connect',
   sealed = 'both',
   array = { rewire_spec },
}

-- Task spec
task_spec = ObjectSpec{
   name = 'Task',
   type = Task,
   sealed = 'both',
   dict = {
      dsl_version = application_dsl_version_spec,
      uri = StringSpec{},
      name = StringSpec{},
      package = rospack_spec,
      vkc = vkc_spec,
      cc = cc_spec,
      fsm = fsm_spec,
      config = config_spec,
   },
   -- We make all optional and check combinations in postcheck.
   optional = { "package", "vkc", "cc", "fsm", "config" }
}

-- task setting
function task_setting_check_combinations(self, obj, vres)
  if (obj.robot and not (obj.o1 or obj.o2) ) or (obj.o1 and obj.o2) then
    return true
  else
    umf.add_msg(vres, "err",[[
invalid combination of task setting fields. Legal combinations:
(robot AND o1 AND o2) OR (o1 AND o2) OR (robot)
]])
    return false
  end
end

task_setting_spec = ObjectSpec{
   name = 'task_setting',
   type = TaskSetting,
   sealed = 'both',
   dict = {
      task = StringSpec{},
      weight = NumberSpec{min=0},
      priority = NumberSpec{min=1},
      o1 = StringSpec{},
      o2 = StringSpec{},
      robot = StringSpec{}
   },
   optional = {"o1","o2","robot"},
   postcheck=task_setting_check_combinations,
}

-- Solver
solver_spec = ObjectSpec{
   name = 'solver_spec',
   type = Solver,
   sealed = 'both',
   dict = {
      name=StringSpec{},
      type=StringSpec{},
      package=rospack_spec,
      config = config_spec
   },
   optional = {"config"}
}


-- CBP spec
cbp_spec = ObjectSpec {
   name = 'cbp_spec',
   type = CBP,
   sealed = 'both',
   dict = {
      -- The following accept tables where the value of any key can be
      -- the respective spec listed in __other:
      dsl_version = cbp_dsl_version_spec,
      name = StringSpec{},
      uri = StringSpec{},
      fsm = fsm_spec,
      robots = TableSpec{ name='robots', sealed='both', array={ robot_spec } },
      objects = TableSpec{ name='objects', sealed='both', array={ object_spec } },
      scene = TableSpec{ name='scene', sealed='both', array={ scene_element_spec } },
      tasks = TableSpec{ name='tasks', sealed='both', array={ task_spec } },
      task_settings = TableSpec{ name='task_settings', sealed='both', array={ task_setting_spec} },
      solver = solver_spec,
   },
   optional = {"objects"}
}

setpoint_generator_spec = ObjectSpec {
   name='setpoint_generator_spec',
   type=SetpointGenerator,
   dict = {
      name=StringSpec{},
      type=StringSpec{},
      package = rospack_spec,
      config = SPconfig_spec,
      task = StringSpec{},
      file = LuaFileSpec{} 
   },
   optional = { "config","file" },
}

-- driver post check
function driver_postcheck(self, obj, vres)
  if (obj.robot and obj.object) then
    umf.add_msg(vres, "err",[[
invalid combination of driver fields: driver can only relate to a robot OR an object
]])
    return false
  end
  return true
end

driver_spec = ObjectSpec {
   name='driver_spec',
   type=Platform,
   dict = {
      name=StringSpec{},
      type=StringSpec{},
      package = rospack_spec,
      config = config_spec,
      robot = StringSpec{},
      object = StringSpec{},
      file = LuaFileSpec{} 
   },
   optional = { "config","type","package", "robot", "object", "file" },
   postcheck = driver_postcheck
}

-- Application level checking.
function foreach(f, tab)
   if not tab then return end
   for i,v in pairs(tab) do f(v,i) end
end

--- Copied  from utils.
--- Pre-order tree traversal.
-- @param fun function to apply to each node
-- @param root root to start from
-- @param pred predicate that nodes must be satisfied for function application.
-- @return table of return values
function maptree(fun, root, pred)
   local res = {}
   local function __maptree(tab)
      --v = a value of the table, k = index/identifier of that value, tab = table to apply maptree on = table that contains k,v pairs
      foreach(function(v, k)
		 if not is_meta(k) then
		    if not pred or pred(v) then res[#res+1] = fun(v, tab, k) end
		    if type(v) == 'table' then __maptree(v) end
		 end
	      end, tab)
   end
   __maptree(root)
   return res
end

--- Build a unique name->obj tab.
-- Raise an error and return false if non-unique keys found.
function build_name_obj_tab(model, vres)
   local ret = true
   local names = {}
   maptree(function (v)
	      if v.name then
		 if not names[v.name] then
		    names[v.name]=v
		 else
		    umf.add_msg(vres, "err", "duplicate use of name "..ts(v.name))
		    ret = false
		 end
	      end
	   end, model, umf.uoo_type)
   return ret, names
end

--- Resolve links
-- @param parent_table object table in which key shall be resolved
-- @param key string key to be resolved
-- @param name_obj_tab table of key value pairs
function resolve(parent_table, key, name_obj_tab)
   local val = name_obj_tab[parent_table[key]]
   if not val then return false end
   key = "_"..key.."ref"
   parent_table[key] = val
   return true
end

--- generate object frame table
-- @param obj object that has object frames
-- @param objFr object frame (o1 or o2)
-- @param name_obj table of name-object pairs
-- @param onr object number (o1 or o2)
-- @param vres
function generate_object_frame_table(obj, objFr, name_obj, onr, vres)
    local _,_,RobotName, FrameName=string.find(objFr, "(.+)%.(.+)")
    -- if the robot exists
    if name_obj[RobotName] then
      -- create the object frame table if it doesn't exist yet
      if not name_obj[RobotName]._ObjectFrames then
        name_obj[RobotName]._ObjectFrames={}
      end
      -- create a robot ref
      local key = "_robotobj_"..onr.."_ref"
      obj[key]=name_obj[RobotName]
      -- if the object frame wasn't in the table yet
      if not name_obj[RobotName]._ObjectFrames[FrameName] then
        -- add the frame to the table
        name_obj[RobotName]._ObjectFrames[FrameName]=RobotName.."_"..FrameName
      end
    else
      umf.add_msg(vres, "err", "Robot or Object "..RobotName.." does not exist: can't add object frame "..FrameName.." to it")
      ret = false
    end
end

--- construct parent links
-- this modifies the model by inserting in each table a link to its parent table
local function add_parent_links(model)
   model._parent = model
   maptree(function (s, p, k) s._parent = p end, model, is_table_notFrame)
end

-- This function creates the name-object table, and resolves links
-- @param self
-- @param model
-- @param vres
-- @return true or false
function post_process_model(self, model, vres)
   local ret, name_obj_tab = build_name_obj_tab(model, vres)
   if not ret then return false end

   -- add references to parent table of each table
   add_parent_links(model)

   -- Resolve Rewire src and tgt. OBSOLETE
   maptree(function (rw)
          --refers "src" to an object in the model?
	      if not resolve(rw, "src", name_obj_tab) then
            --refers "src" to an entry of the parent connect table? 
            if not resolve(rw, "src", rw._parent._parent) then
		      umf.add_msg(vres, "err", "failed to resolve Rewire.src: "..ts(rw.src).." from "..ts(rw._parent._parent.name))
		      ret = false
            end
	      end
          --refers "tgt" to an object in the model?
	      if not resolve(rw, "tgt", name_obj_tab) then
            --refers "tgt" to an entry of the parent of the connect table? 
            if not resolve(rw, "tgt", rw._parent._parent) then
		      umf.add_msg(vres, "err", "failed to resolve Rewire.tgt: "..ts(rw.tgt).." from instance of "..ts(rw._parent._parent:class()).." with name "..ts(rw._parent._parent.name))
		      ret = false
            end
	      end
	   end, model, is_Rewire)

   -- Resolve robot and object
   maptree(function (robobj)
     if robobj.robot then
       local robot_name = robobj.robot
       --refers "robot" to a robot?
       if not resolve(robobj, "robot", name_obj_tab) then
         umf.add_msg(vres, "err", "failed to resolve "..ts(robobj:class().name)..".robot: "..ts(robobj.robot))
         ret = false
       else if not instance_of(Robot, robobj._robotref) then
           umf.add_msg(vres, "err", ts(robobj:class().name)..".robot: "..robot_name..", is not the name of a robot")
           ret = false
         end
       end
     end
     if robobj.object then
       local object_name = robobj.object
       --refers "object" to an object?
       if not resolve(robobj, "object", name_obj_tab) then
         umf.add_msg(vres, "err", "failed to resolve "..ts(robobj:class().name)..".object: "..ts(robobj.object))
         ret = false
       else if not instance_of(Object, robobj._objectref) then
           umf.add_msg(vres, "err", ts(obj:class().name)..".object: "..object_name..", is not the name of an object")
           ret = false
         end
       end
       
     end
   end, model, is_SceneElement_TaskSetting_Platform )

   -- Resolve task of TaskSetting
   maptree(function (tskset)
     if tskset.task then
       local task_name = tskset.task
       --refers "task" to a task?
       if not resolve(tskset, "task", name_obj_tab) then
         umf.add_msg(vres, "err", "failed to resolve "..ts(tskset:class().name)..".task: "..ts(tskset.task))
         ret = false
       else if not instance_of(Task, tskset._taskref) then
           umf.add_msg(vres, "err", ts(tskset:class().name)..".task: "..task_name..", is not the name of a task")
           ret = false
         end
       end
     end
   end, model, is_TaskSetting)

   -- Resolve task of SetpointGenerator
   maptree(function (tskset)
     if tskset.task then
       local task_name = tskset.task
       --refers "task" to a task?
       if not resolve(tskset, "task", name_obj_tab) then
         umf.add_msg(vres, "err", "failed to resolve "..ts(tskset.name)..".task: "..ts(tskset.task))
         ret = false
       else if not instance_of(Task, tskset._taskref) then
           umf.add_msg(vres, "err", ts(tskset.name)..".task: "..task_name..", is not the name of a task")
           ret = false
         end
       end
     end
   end, model, is_SetpointGenerator)

   -- List object frames
   maptree(function (v)
     if v.o1 then generate_object_frame_table(v, v.o1, name_obj_tab, "o1", vres) end
     if v.o2 then generate_object_frame_table(v, v.o2, name_obj_tab, "o2", vres) end
   end, model, is_TaskSetting)

   return ret
end


--- Application
Application_spec = ObjectSpec {
   name = 'application',
   type = Application,
   sealed=false,
   dict={
      name = StringSpec{},
      dsl_version = application_dsl_version_spec,
      uri = StringSpec{},
      fsm = fsm_spec,
      cbp = cbp_spec,
      setpoint_generators = TableSpec{
	 name='setpoint_generators',
	 sealed='both',
	 array={ setpoint_generator_spec },
      },
      drivers = TableSpec{
	 name='drivers',
	 sealed='both',
	 array={ driver_spec },
      },
   },
   optional={'cbp','setpoint_generators'},
   postcheck=post_process_model,
}
