#!/bin/bash
# Create a template for a new itasc application model

if (( $# != 1 ));
then
	echo "Not enough parameters given. Usage: $@ <modelname>"
	exit 1
fi


MODELNAME=$1

if [ -d $MODELNAME ];
then
	echo "Model with name '$MODELNAME' already exists. Exiting..."
	exit 1
fi

echo "Creating a template model, named $MODELNAME.lua..."
cp model_template.lua $MODELNAME.lua




echo "Creating simple executable (using iTaSC DSL to Orocos Deployer) for the model, named run_$MODELNAME.sh..."
echo "with following options: "
echo "  log level: warning,"
echo "  browser type: task browser (default Orocos),"
echo "  assumption that model is in itasc_dsl package"
f=run_$MODELNAME.sh
echo "#!/bin/bash" > $f
echo "rosrun ocl deployer-gnulinux -lwarning -s \`rospack find itasc_dsl_orocos_deployer\`/itasc_deploy_taskbrowser.ops -- file://itasc_dsl#/$MODELNAME.lua" >> $f
chmod +x $f

echo ""
echo "Ok, all done."
